const mongoose = require("mongoose");

const productItemSchema = new mongoose.Schema({
    productItemName: {
        type: String,
        required: [true, "Product/Item name is required"]
    },
    productItemDescription: {
        type: String,
        required: [true, "Product/Item description is required"]
    },
    productItemPrice: {
        type: String,
        required: [true, "Product/Item price is required"]
    },
    productItemStocks: {
        type: Number,
        required: [true, "Product/Item number of stocks is required"]
    },
    productItemIsActive: {
        type: Boolean,
        default: true
    },
    productItemSetUpOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            ordersID: {
                type: String,
                required: [true, "orderID is required"]
            },
            ordersUserID: {
                type: String,
                required: [true, "UserId is required"]
            },
            //optional field
            ordersUserEmail: {
                type: String,
                required: [true, "UserEmail is required"]
            },
            ordersQuantity: {
                type: Number,
                required: [true, "quantity is required"]
            },
            ordersProductsPurchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
})


module.exports = mongoose.model("ProductItem", productItemSchema);
